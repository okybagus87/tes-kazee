var alertUtils = {
    showSuccess: (message, timeout) => {
        $('#success-message').html(message)
        if(timeout != undefined){
            setTimeout(() => {
                $('#alert-success-close').click()
            }, timeout)
        }

        $('#alert-success').show()
    },
    showFailed: (message, timeout) => {
        $('#danger-message').html(message)
        if(timeout !== undefined){
            setTimeout(() => {
                $('#alert-danger-close').click()
            }, timeout)
        }

        $('#alert-danger').show()
    },
    showFailedAdd: (message, timeout) => {
        $('#add-danger-message').html(message)
        if(timeout !== undefined){
            setTimeout(() => {
                $('#add-alert-danger-close').click()
            }, timeout)
        }

        $('#add-alert-danger').show()
    },
    showFailedEdit: (message, timeout) => {
        $('#edit-danger-message').html(message)
        if(timeout !== undefined){
            setTimeout(() => {
                $('#edit-alert-danger-close').click()
            }, timeout)
        }

        $('#edit-alert-danger').show()
    }
}

$('#alert-success-close').click(function (e) {
    $('#alert-success').hide()
});

$('#alert-danger-close').click(function () {
    $('#alert-danger').hide()
});

$('#add-alert-danger-close').click(function () {
    $('#add-alert-danger').hide()
});

$('#edit-alert-danger-close').click(function () {
    $('#edit-alert-danger').hide()
});

loadingUtils = {
    btnBlock: selector => {
        $(selector).addClass(['btn-loading', 'disabled'])
    },
    btnUnblock: (selector) => {
        $(selector).removeClass(['btn-loading', 'disabled'])
    },
    pageBlock: selector => {
        // $(selector).addClass(['btn-loading', 'disabled'])
    },
    pageUnblock: (selector) => {
        // $(selector).removeClass(['btn-loading', 'disabled'])
    },
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

if($('.select2').length){
    $('.select2').select2({
        dropdownCssClass: 'hover-primary',
        minimumResultsForSearch: '',
        width: '100%'
    });
}

if($('.dropify').length){
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        }
    });
}
