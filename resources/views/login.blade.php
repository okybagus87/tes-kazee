@extends('layouts.master2')
@section('css')
@endsection
@section('content')
    <div class="page">
        <div class="page-content">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="">
                            <div class="text-white">
                                <div class="card-body">
                                    <h2 class="display-4 mb-2 font-weight-bold text-center"><strong>Login</strong></h2>
                                    <h4 class="text-white-80 mb-7 text-center">Sign In to your account</h4>
                                    <div class="row">
                                        <div class="col-9 d-block mx-auto">
                                            <div class="alert alert-danger show fade" id="alert-danger" role="alert"
                                                style="display: none"><button type="button" class="close"
                                                    aria-hidden="true" id="alert-danger-close">×</button><span
                                                    id="danger-message">-</span></div>
                                            <form id="login-form">
                                                @csrf
                                                <div class="input-group mb-4">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fe fe-user"></i>
                                                        </div>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Username"
                                                        name="username">
                                                </div>
                                                <div class="input-group mb-4">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text">
                                                            <i class="fe fe-lock"></i>
                                                        </div>
                                                    </div>
                                                    <input type="password" class="form-control" placeholder="Password"
                                                        name="password">
                                                </div>
                                            </form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <button type="button" class="btn btn-danger btn-block px-4"
                                                        id="login-button">Login</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-none d-md-flex align-items-center">
                        <img src="{{ URL::asset('assets/images/photos/1.jpg') }}" alt="img">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function initCreateForm() {
            var form = $('#login-form');

            var validator = form.validate({
                errorClass: 'invalid-feedback text-white',
                errorElement: 'div',
                errorPlacement: (error, element) => {
                    error.insertAfter(element)
                },
                highlight: element => {
                    $(element).addClass(['is-invalid', 'state-invalid', 'border-danger'])
                },
                unhighlight: element => {
                    $(element).removeClass(['is-invalid', 'state-invalid', 'border-danger'])
                },
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                }
            })

            $('#login-button').click(function() {
                var isValid = form.valid();
                if (isValid) {
                    loadingUtils.btnBlock('#login-button');
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('auth.login') }}',
                        data: $('#login-form').serialize(),
                        dataType: 'json'
                    }).done(function(response) {
                        if (response.status) {
                            window.location.href = response.redirect
                        }
                    }).fail(function(err) {
                        jsonErr = err.responseJSON;

                        if ('errors' in jsonErr) {
                            message = [];
                            Object.entries(jsonErr.errors).forEach(([key, val]) => {
                                message.push(val.join('<br>'))
                            });

                            alertUtils.showFailed(message.join('<br>'))
                        } else if ('message' in jsonErr) {
                            alertUtils.showFailed(jsonErr.message)
                        } else {
                            alertUtils.showFailed('Failed connecting to serve, please try again')
                        }
                    }).always(function() {
                        loadingUtils.btnUnblock('#login-button');
                    })
                }
            })
        }

        $(document).ready(function() {
            initCreateForm()
        });
    </script>
@endsection
