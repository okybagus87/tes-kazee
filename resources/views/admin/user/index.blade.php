@extends('layouts.master')
@section('page-title', 'User')
@section('css')
    <!-- Data table css -->
    <link href="{{ URL::asset('assets/plugins/datatable/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/plugins/datatable/responsive.bootstrap4.min.css') }}" rel="stylesheet" />

    <!-- INTERNAL Select2 css -->
    <link href="{{ URL::asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
    <style>
        .select2-search__field {
            width: 100% !important;
        }
    </style>
@endsection
@section('page-header')
    <!--Page header-->
    <div class="page-header">
        <div class="page-leftheader">
            <h4 class="page-title mb-0">User</h4>
        </div>
    </div>
    <!--End Page header-->
@endsection
@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <!--table-->
            <div class="card">
                <div class="card-header">
                    <div class="card-title">User List</div>
                    <div class="card-options">
                        <div class="btn-group p-0">
                            <button class="btn btn-info" type="button" data-toggle="modal"
                                data-target="#modal-add">Add</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="alert alert-success show fade" id="alert-success" role="alert" style="display: none">
                        <button type="button" class="close" aria-hidden="true" id="alert-success-close">×</button><span
                            id="success-message">-</span>
                    </div>
                    <div class="alert alert-danger show fade" id="alert-danger" role="alert" style="display: none"><button
                            type="button" class="close" aria-hidden="true" id="alert-danger-close">×</button><span
                            id="danger-message">-</span></div>
                    <div class="table-responsive">
                        <table class="table table-vcenter table-bordered text-nowrap w-100" id="datatable">
                        </table>
                    </div>
                </div>
            </div>
            <!--/table-->
        </div>
    </div>
    <!-- /Row -->
    </div>
    @include('admin.user._add')
    @include('admin.user._edit')
    @include('admin.user._group')
    </div><!-- end app-content-->
    </div>
@endsection
@section('js')
    @include('admin.user._script')
@endsection
