<div class="modal fade" id="modal-group" tabindex="-1" aria-labelledby="modal-group-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-group-title">User Group</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="group-user-form" class="w-100">
                        <input type="hidden" name="id" id="group-id">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="group-group" class="form-label">Groups</label>
                                <select name="group_id[]" id="group-group" class="form-control select2"
                                    data-placeholder="Choose one/many" multiple>
                                    @foreach ($groups as $group)
                                        <option value="{{ $group->id }}">{{ ucwords($group->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="col-12">
                        <div class="alert alert-danger show fade" id="group-alert-danger" role="alert"
                            style="display: none"><button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true" id="group-alert-danger-close">×</button><span
                                id="group-danger-message">-</span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" id="btn-group-submit">Save</button>
            </div>
        </div>
    </div>
</div>
