{{-- datatable --}}
<script src="{{ URL::asset('assets/plugins/datatable/js/jquery.dataTables.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/datatable/responsive.bootstrap4.min.js') }}"></script>

<!-- INTERNAL Select2 js -->
<script src="{{ URL::asset('assets/plugins/select2/select2.full.min.js') }}"></script>
<script>
    var table, formAdd, formEdit, formGroup;

    function initDataTable() {
        table = $('#datatable').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.user.datatable') }}",
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_',
            },
            columns: [{
                    data: 'id',
                    name: 'users.id',
                    title: 'Id'
                },
                {
                    data: 'username',
                    name: 'users.username',
                    title: 'Username',
                    render: $.fn.dataTable.render.text()
                },
                {
                    data: 'name',
                    name: 'users.name',
                    title: 'Name',
                    render: $.fn.dataTable.render.text()
                },
                {
                    data: 'groups',
                    name: 'groups.name',
                    title: 'Group/s',
                    render: $.fn.dataTable.render.text()
                },
                {
                    data: 'created_at',
                    name: 'users.created_at',
                    title: 'Created At'
                },
                {
                    data: 'updated_at',
                    name: 'users.updated_at',
                    title: 'Updated At'
                },
                {
                    data: null,
                    title: 'action',
                    responsivePriority: 1,
                    orderable: false,
                    searchable: false,
                    render: (data, type, row) => {
                        result = '';
                        result = result +
                            `<span data-id="${row.id}" href="#" class="btn btn-info btn-sm mr-1 btn-user-group" title="Group" ><i class="fa fa-users" style="cursor:pointer;"></i> | Group</span><span data-id="${row.id}" href="#" class="btn btn-warning btn-sm mr-1 btn-edit" title="Edit" ><i class="fa fa-edit" style="cursor:pointer;"></i> | Edit</span><span data-id="${row.id}" href="#" class="btn btn-danger btn-sm mr-1 btn-delete" title="Delete" ><i class="fa fa-trash" style="cursor:pointer;"></i> | Delete</span>`;
                        return result;
                    }
                },
            ]
        });

        table.on('draw', function() {
            initDTEvents()
        })
    }

    function initDTEvents() {
        $('.btn-delete').on('click', function() {
            swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it',
                confirmButtonColor: '#ed2027',
            }).then(result => {
                if (result.value) {
                    $(this).addClass(['disabled', 'btn-loading']);

                    var targetId = $(this).data('id');
                    var url = '{{ route('admin.user.delete', ['user_id' => ':id']) }}';
                    url = url.replace(':id', targetId);
                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        dataType: 'json'
                    }).done(function(response) {
                        if (response.status) {
                            table.ajax.reload();
                            alertUtils.showSuccess('User deleted', 5000)
                        }
                    }).fail(function(err) {
                        jsonErr = err.responseJSON;

                        if ('errors' in jsonErr) {
                            message = [];
                            Object.entries(jsonErr.errors).forEach(([key, val]) => {
                                message.push(val.join('<br>'))
                            });

                            alertUtils.showFailed(message.join('<br>'))
                        } else if ('message' in jsonErr) {
                            alertUtils.showFailed(jsonErr.message)
                        } else {
                            alertUtils.showFailed(
                                'Failed connecting to serve, please try again')
                        }
                    }).always(function() {
                        $('.btn-delete').removeClass(['disabled', 'btn-loading'])
                    })

                }
            })
        });

        $('.btn-edit').on('click', function() {
            targetId = $(this).data('id');
            url = '{{ route('admin.user.get', ['user_id' => ':id']) }}';
            url = url.replace(':id', targetId);

            loadingUtils.btnBlock(this);
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json'
            }).done(function(response) {
                if (response.status) {
                    formEdit.populateForm(response.data);
                    $('#modal-edit').modal('show')
                }
            }).fail(function(err) {
                jsonErr = err.responseJSON;

                if ('errors' in jsonErr) {
                    message = [];
                    Object.entries(jsonErr.errors).forEach(([key, val]) => {
                        message.push(val.join('<br>'))
                    });

                    alertUtils.showFailed(message.join('<br>'))
                } else if ('message' in jsonErr) {
                    alertUtils.showFailed(jsonErr.message)
                } else {
                    alertUtils.showFailed('Failed connecting to serve, please try again')
                }
            }).always(function() {
                loadingUtils.btnUnblock('.btn-edit')
            })
        });

        $('.btn-user-group').on('click', function() {
            targetId = $(this).data('id');
            url = '{{ route('admin.user.get-user-group', ['user_id' => ':id']) }}';
            url = url.replace(':id', targetId);

            loadingUtils.btnBlock(this);
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json'
            }).done(function(response) {
                if (response.status) {
                    formGroup.populateForm(response.data);
                    $('#modal-group').modal('show')
                }
            }).fail(function(err) {
                jsonErr = err.responseJSON;

                if ('errors' in jsonErr) {
                    message = [];
                    Object.entries(jsonErr.errors).forEach(([key, val]) => {
                        message.push(val.join('<br>'))
                    });

                    alertUtils.showFailed(message.join('<br>'))
                } else if ('message' in jsonErr) {
                    alertUtils.showFailed(jsonErr.message)
                } else {
                    alertUtils.showFailed('Failed connecting to serve, please try again')
                }
            }).always(function() {
                loadingUtils.btnUnblock('.btn-user-group')
            })
        })
    }

    function initCreateForm() {
        var form = $('#add-user-form');
        var modal = $('#modal-add');

        var validator = form.validate({
            errorClass: 'invalid-feedback',
            errorElement: 'div',
            errorPlacement: (error, element) => {
                error.insertAfter(element)
            },
            highlight: element => {
                $(element).addClass(['is-invalid', 'state-invalid', 'border-danger'])
            },
            unhighlight: element => {
                $(element).removeClass(['is-invalid', 'state-invalid', 'border-danger'])
            },
            rules: {
                username: {
                    required: true,
                    maxlength: 100
                },
                name: {
                    required: true,
                    maxlength: 100
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                },
                password: {
                    required: true,
                    maxlength: 255
                }
            }
        });

        $('#btn-add-submit').click(function() {
            var isValid = form.valid();
            if (isValid) {
                loadingUtils.btnBlock(this);
                $.ajax({
                    type: 'POST',
                    url: '{{ route('admin.user.create') }}',
                    data: form.serialize(),
                    dataType: 'json',
                }).done(function(response) {
                    if (response.status) {
                        modal.modal('hide');
                        table.ajax.reload();
                        alertUtils.showSuccess('User created', 5000)
                    }
                }).fail(function(err) {
                    jsonErr = err.responseJSON;

                    if ('errors' in jsonErr) {
                        message = [];
                        Object.entries(jsonErr.errors).forEach(([key, val]) => {
                            message.push(val.join('<br>'))
                        });

                        alertUtils.showFailedAdd(message.join('<br>'))
                    } else if ('message' in jsonErr) {
                        alertUtils.showFailedAdd(jsonErr.message)
                    } else {
                        alertUtils.showFailedAdd('Failed connecting to serve, please try again')
                    }
                }).always(function() {
                    loadingUtils.btnUnblock('#btn-add-submit')
                })
            }
        });

        modal.on('hide.bs.modal', function() {
            validator.resetForm();
            form.trigger('reset')
        });

        modal.on('hidden.bs.modal', function() {
            $('#add-role').val(null).trigger('change')
        });

        return {
            validator: validator
        }
    }

    function initEditForm() {
        var form = $('#edit-user-form');
        var modal = $('#modal-edit');

        var requiredPass = false;

        var validator = form.validate({
            errorClass: 'invalid-feedback',
            errorElement: 'div',
            errorPlacement: (error, element) => {
                error.insertAfter(element)
            },
            highlight: element => {
                $(element).addClass(['is-invalid', 'state-invalid', 'border-danger'])
            },
            unhighlight: element => {
                $(element).removeClass(['is-invalid', 'state-invalid', 'border-danger'])
            },
            rules: {
                id: {
                    required: true,
                    digits: true
                },
                username: {
                    required: true,
                    maxlength: 100
                },
                name: {
                    required: true,
                    maxlength: 100
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                },
                password: {
                    required: () => {
                        return requiredPass;
                    },
                    maxlength: 255
                }
            }
        });

        $('#btn-edit-submit').click(function() {
            var isValid = form.valid();
            if (isValid) {
                url = '{{ route('admin.user.update', ['user_id' => ':id']) }}';
                url = url.replace(':id', $('#edit-id').val());
                loadingUtils.btnBlock(this);
                $.ajax({
                    type: 'PUT',
                    url: url,
                    data: form.serialize(),
                    dataType: 'json',
                }).done(function(response) {
                    if (response.status) {
                        modal.modal('hide');
                        table.ajax.reload();
                        alertUtils.showSuccess('User updated', 5000)
                    }
                }).fail(function(err) {
                    jsonErr = err.responseJSON;

                    if ('errors' in jsonErr) {
                        message = [];
                        Object.entries(jsonErr.errors).forEach(([key, val]) => {
                            message.push(val.join('<br>'))
                        });

                        alertUtils.showFailedEdit(message.join('<br>'))
                    } else if ('message' in jsonErr) {
                        alertUtils.showFailedEdit(jsonErr.message)
                    } else {
                        alertUtils.showFailedEdit('Failed connecting to serve, please try again')
                    }
                }).always(function() {
                    loadingUtils.btnUnblock('#btn-edit-submit')
                })
            }
        });

        modal.on('hide.bs.modal', function() {
            validator.resetForm();
            form.trigger('reset')
        });

        modal.on('hidden.bs.modal', function() {
            $('#edit-role').val(null).trigger('change')
        });

        var populateForm = data => {
            $('#edit-id').val(data.id);
            $('#edit-username').val(data.username);
            $('#edit-name').val(data.name);
            $('#edit-email').val(data.email);

            if (data.email) {
                requiredPass = false
            } else {
                requiredPass = true
            }
        };

        return {
            validator: validator,
            populateForm: populateForm
        }
    }

    function initGroupForm() {
        var form = $('#group-user-form');
        var modal = $('#modal-group');

        var validator = form.validate({
            errorClass: 'invalid-feedback',
            errorElement: 'div',
            errorPlacement: (error, element) => {
                if (element.hasClass('select2'))
                    error.insertAfter(element.next('span'));
                else
                    error.insertAfter(element);
            },
            highlight: element => {
                if ($(element).hasClass('select2')) {
                    $(element).next('span').find('.select2-selection').attr('style',
                        'border: 1px solid rgb(220, 4, 65) !important');
                } else {
                    $(element).addClass(['is-invalid', 'state-invalid', 'border-danger'])
                }
            },
            unhighlight: element => {
                if ($(element).hasClass('select2')) {
                    $(element).next('span').find('.select2-selection').removeAttr('style');
                } else {
                    $(element).removeClass(['is-invalid', 'state-invalid', 'border-danger'])
                }
            },
            rules: {
                id: {
                    required: true,
                },
                group: {
                    required: true,
                }
            }
        });

        $('#btn-group-submit').click(function() {
            var isValid = form.valid();
            if (isValid) {
                url = '{{ route('admin.user.update-user-group', ['user_id' => ':id']) }}';
                url = url.replace(':id', $('#group-id').val());
                loadingUtils.btnBlock(this);
                $.ajax({
                    type: 'PUT',
                    url: url,
                    data: form.serialize(),
                    dataType: 'json'
                }).done(function(response) {
                    if (response.status) {
                        modal.modal('hide');
                        table.ajax.reload();
                        alertUtils.showSuccess('User updated', 5000)
                    }
                }).fail(function(err) {
                    jsonErr = err.responseJSON;

                    if ('errors' in jsonErr) {
                        message = [];
                        Object.entries(jsonErr.errors).forEach(([key, val]) => {
                            message.push(val.join('<br>'))
                        });

                        alertUtils.showFailedGroup(message.join('<br>'))
                    } else if ('message' in jsonErr) {
                        alertUtils.showFailedGroup(jsonErr.message)
                    } else {
                        alertUtils.showFailedGroup('Failed connecting to serve, please try again')
                    }
                }).always(function() {
                    loadingUtils.btnUnblock('#btn-group-submit')
                })
            }
        });

        $('#group-group').on('change', function() {
            $(this).valid()
        });

        modal.on('hide.bs.modal', function() {
            validator.resetForm();
            form.trigger('reset')
        });

        modal.on('hidden.bs.modal', function() {
            $('#group-group').val(null).trigger('change')
        });

        var populateForm = data => {
            $('#group-id').val(data.id);
            $('#group-group').val(data.group_ids).trigger('change');
        };

        return {
            validator: validator,
            populateForm: populateForm
        }
    }

    function addAlerts() {
        alertUtils.showFailedGroup = (message, timeout) => {
            $('#group-danger-message').html(message);
            if (timeout !== undefined) {
                setTimeout(() => {
                    $('#group-alert-danger-close').click()
                }, timeout)
            }

            $('#group-alert-danger').show()
        };

        $('#group-alert-danger-close').click(function() {
            $('#group-alert-danger').hide()
        })
    }

    $(document).ready(function() {
        addAlerts();
        initDataTable();
        initDTEvents();
        formAdd = initCreateForm();
        formEdit = initEditForm();
        formGroup = initGroupForm();
    })
</script>
