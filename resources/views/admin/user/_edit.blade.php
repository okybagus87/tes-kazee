<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="modal-edit-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-edit-title">User Edit</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="edit-user-form" class="w-100">
                        <input type="hidden" name="id" id="edit-id">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="edit-username" class="form-label">Username</label>
                                <input type="text" class="form-control" name="username" id="edit-username"
                                    placeholder="Username">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="edit-name" class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" id="edit-name"
                                    placeholder="Name">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="edit-password" class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" id="edit-password"
                                    placeholder="password">
                            </div>
                        </div>
                    </form>
                    <div class="col-12">
                        <div class="alert alert-danger show fade" id="edit-alert-danger" role="alert"
                            style="display: none"><button type="button" class="close" aria-hidden="true"
                                id="edit-alert-danger-close">×</button><span id="edit-danger-message">-</span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" id="btn-edit-submit">Save</button>
            </div>
        </div>
    </div>
</div>
