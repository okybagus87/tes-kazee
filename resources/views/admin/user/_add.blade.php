<div class="modal fade" id="modal-add" tabindex="-1" aria-labelledby="modal-add-title" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-add-title">User Add</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="add-user-form" class="w-100">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add-username" class="form-label">Username</label>
                                <input type="text" class="form-control" name="username" id="add-username"
                                    placeholder="Username">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add-name" class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" id="add-name"
                                    placeholder="Name">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add-password" class="form-label">Password</label>
                                <input type="password" class="form-control" name="password" id="add-password"
                                    placeholder="Password">
                            </div>
                        </div>
                    </form>
                    <div class="col-12">
                        <div class="alert alert-danger show fade" id="add-alert-danger" role="alert"
                            style="display: none"><button type="button" class="close" aria-hidden="true"
                                id="add-alert-danger-close">×</button><span id="add-danger-message">-</span></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-info" id="btn-add-submit">Save</button>
            </div>
        </div>
    </div>
</div>
