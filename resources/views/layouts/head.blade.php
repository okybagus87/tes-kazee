  <!-- Title -->
  <title>{{ config('app.name') }} | @yield('page-title')</title>

  <!--Favicon -->
  <link rel="icon" href="/favicon.ico" type="image/x-icon" />

  <!-- App css-->
  <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">

  @yield('css')

  <!-- Style css -->
  <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet" />
  <link href="{{ URL::asset('assets/css/dark.css') }}" rel="stylesheet" />
  <link href="{{ URL::asset('assets/css/skin-modes.css') }}" rel="stylesheet" />

  <!-- Animate css -->
  <link href="{{ URL::asset('assets/css/animated.css') }}" rel="stylesheet" />

  <!--Sidemenu css -->
  <link href="{{ URL::asset('assets/css/sidemenu.css') }}" rel="stylesheet">

  <!-- P-scroll bar css-->
  <link href="{{ URL::asset('assets/plugins/p-scrollbar/p-scrollbar.css') }}" rel="stylesheet" />

  <!---Icons css-->
  <link href="{{ URL::asset('assets/iconfonts/typicons/typicons.css') }}" rel="stylesheet" />

  <!-- Simplebar css -->
  <link rel="stylesheet" href="{{ URL::asset('assets/plugins/simplebar/css/simplebar.css') }}">

  <!-- Color Skin css -->
  <link id="theme" href="{{ URL::asset('assets/colors/color1.css') }}" rel="stylesheet" type="text/css" />
