    <aside class="app-sidebar">
        <div class="app-sidebar__logo">
            <a class="header-brand" href="{{ route('admin.dashboard') }}">
                <img src="" class="header-brand-img desktop-lgo" style="width: 50px; height: 50px;" alt="Logo">
                <img src="" class="header-brand-img dark-logo" style="width: 50px; height: 50px;" alt="Logo">
                <img src="" class="header-brand-img mobile-logo" alt="Logo">
                <img src="" class="header-brand-img darkmobile-logo" alt="Logo">
            </a>
        </div>
        <div class="app-sidebar__user">
            <div class="dropdown user-pro-body text-center">
                <div class="user-pic">
                    <img src="{{ URL::asset('assets/images/users/1.jpg') }}" alt="user-img"
                        class="avatar-xl rounded-circle mb-1">
                </div>
            </div>
        </div>
        <ul class="side-menu app-sidebar3">
            <li class="slide">
                <a class="side-menu__item" href="{{ route('admin.dashboard') }}">
                    <span class="side-menu__icon bg-white mx-auto d-flex align-items-center justify-content-center"><i
                            class="fa fa-dashboard" style="font-size: 1rem"></i></span>
                    <span class="side-menu__label ml-3">Dashboard</span>
                </a>
            </li>
            <li class="slide">
                <a class="side-menu__item" href="{{ route('admin.user.index') }}">
                    <span class="side-menu__icon bg-white mx-auto d-flex align-items-center justify-content-center"><i
                            class="fa fa-users" style="font-size: 1rem"></i></span>
                    <span class="side-menu__label ml-3">User</span>
                </a>
            </li>
        </ul>
    </aside>
    <!--aside closed-->
