		<!-- Jquery js-->

        <!-- App js-->
        <script src="{{URL::asset('js/app.js')}}"></script>

		<!--Othercharts js-->
		<script src="{{URL::asset('assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>

		<!-- Circle-progress js-->
		<script src="{{URL::asset('assets/js/circle-progress.min.js')}}"></script>

		<!-- Jquery-rating js-->
		<script src="{{URL::asset('assets/plugins/rating/jquery.rating-stars.js')}}"></script>

		@yield('js')

        <!-- Custom js-->
        <script src="{{URL::asset('assets/js/custom.js')}}"></script>
        <script src="{{URL::asset('assets/js/custom-local.js')}}"></script>
