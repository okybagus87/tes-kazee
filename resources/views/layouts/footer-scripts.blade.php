<!-- Back to top -->
<a href="#top" id="back-to-top"><i class="fe fe-chevrons-up"></i></a>

<!-- App js-->
<script src="{{ URL::asset('js/app.js') }}"></script>

<!--Othercharts js-->
<script src="{{ URL::asset('assets/plugins/othercharts/jquery.sparkline.min.js') }}"></script>

<!-- Circle-progress js-->
<script src="{{ URL::asset('assets/js/circle-progress.min.js') }}"></script>

<!-- Jquery-rating js-->
<script src="{{ URL::asset('assets/plugins/rating/jquery.rating-stars.js') }}"></script>

<script src="{{ URL::asset('assets/plugins/sidemenu/sidemenu.js') }}"></script>

<!-- P-scroll js-->
<script src="{{ URL::asset('assets/plugins/p-scrollbar/p-scrollbar.js') }}"></script>
<script src="{{ URL::asset('assets/plugins/p-scrollbar/p-scroll1.js') }}"></script>

<!-- Simplebar JS -->
<script src="{{ URL::asset('assets/plugins/simplebar/js/simplebar.min.js') }}"></script>

@yield('js')

<!-- Custom js-->
<script src="{{ URL::asset('assets/js/custom.js') }}"></script>
<script src="{{ URL::asset('assets/js/custom-local.js') }}"></script>

<script>
    $('#btn-logout').click(function() {
        loadingUtils.btnBlock(this);
        $.ajax({
            type: 'POST',
            url: '{{ route('auth.logout') }}',
            dataType: 'json'
        }).done(function(response) {
            if (response.status) {
                window.location.href = response.redirect
            }
        }).fail(function(err) {
            jsonErr = err.responseJSON;

            if ('errors' in jsonErr) {
                message = [];
                Object.entries(jsonErr.errors).forEach(([key, val]) => {
                    message.push(val.join('<br>'))
                });

                alertUtils.showFailed(message.join('<br>'))
            } else if ('message' in jsonErr) {
                alertUtils.showFailed(jsonErr.message)
            } else {
                alertUtils.showFailed('Failed connecting to serve, please try again')
            }
        }).always(function() {
            loadingUtils.btnUnblock('#btn-logout')
        })

    });
</script>
