  <!-- Title -->
  <title>{{ config('app.name') }}</title>

  <!--Favicon -->
  <link rel="icon" href="{{ URL::asset('assets/images/photos/1.jpg') }}" type="image/x-icon" />

  <!-- App css-->
  <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet" />

  @yield('css')

  <!-- Style css -->
  <link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet" />
  <link href="{{ URL::asset('assets/css/dark.css') }}" rel="stylesheet" />
  <link href="{{ URL::asset('assets/css/skin-modes.css') }}" rel="stylesheet" />

  <!-- Animate css -->
  <link href="{{ URL::asset('assets/css/animated.css') }}" rel="stylesheet" />

  <!---Icons css-->
  <link href="{{ URL::asset('assets/iconfonts/feather/feather.css') }}" rel="stylesheet" />

  <!-- Color Skin css -->
  <link id="theme" href="{{ URL::asset('assets/colors/color1.css') }}" rel="stylesheet" type="text/css" />
