      <!--app header-->
      <div class="app-header header">
          <div class="container-fluid">
              <div class="d-flex">
                  <a class="header-brand align-items-center" href="{{ route('admin.dashboard') }}">
                      <img src="" class="header-brand-img desktop-lgo" alt="Admintro logo">
                      <img src="" class="header-brand-img dark-logo" alt="Admintro logo">
                      <img src="" class="header-brand-img mobile-logo m-0" style="width: 50px; height: 50px;"
                          alt="logo">
                      <img src="" class="header-brand-img darkmobile-logo m-0"
                          style="width: 50px; height: 50px;" alt="logo">
                  </a>
                  <div class="app-sidebar__toggle d-flex align-items-center" data-toggle="sidebar">
                      <a class="open-toggle" href="{{ url('/' . ($page = '#')) }}">
                          <i class="fa fa-bars header-icon bg-white d-flex align-items-center justify-content-center"
                              style="font-size: 1rem"></i>
                      </a>
                  </div>
                  <div class="d-flex order-lg-2 ml-auto align-items-center justify-content-center">
                      <a class="btn" type="button" href="#" id="btn-logout">
                          <i class="fa fa-sign-out mr-1"></i>Sign Out
                      </a>
                  </div>
              </div>
          </div>
      </div>
      <!--/app header-->
