<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', function () {
    if (auth()->check()) {
        return redirect()->route('admin.dashboard');
    }

    return redirect()->route('auth.index');
});

Route::name('auth.')->group(function () {
    Route::get('login', [\App\Http\Controllers\Admin\AuthController::class, 'index'])->name('index');
    Route::post('login', [\App\Http\Controllers\Admin\AuthController::class, 'login'])->name('login');
    Route::post('logout', function () {
        auth()->logout();
        return response(['status' => true, 'redirect' => route('auth.index')]);
    })->name('logout')->middleware('auth');
});

Route::middleware('auth')->group(function () {
    Route::prefix('admin/')->name('admin.')->group(function () {
        Route::get('dashboard', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');

        Route::prefix('user/')->name('user.')->group(function () {
            Route::get('', [\App\Http\Controllers\Admin\UserController::class, 'index'])->name('index');
            Route::post('', [\App\Http\Controllers\Admin\UserController::class, 'create'])->name('create');
            Route::get('datatable', [\App\Http\Controllers\Admin\UserController::class, 'datatable'])->name('datatable');
            Route::get('{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'get'])->name('get');
            Route::put('{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'update'])->name('update');
            Route::delete('{user_id}', [\App\Http\Controllers\Admin\UserController::class, 'delete'])->name('delete');
            Route::get('{user_id}/group', [\App\Http\Controllers\Admin\UserController::class, 'getUserGroup'])->name('get-user-group');
            Route::put('{user_id}/group', [\App\Http\Controllers\Admin\UserController::class, 'updateUserGroup'])->name('update-user-group');
        });
    });
});
