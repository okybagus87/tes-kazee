<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Group::create([
            'name' => 'administrator'
        ]);

        User::create([
            'username' => 'admin',
            'name' => 'administrator',
            'password' => bcrypt('admin'),
        ])->groups()->attach(1);
    }
}
