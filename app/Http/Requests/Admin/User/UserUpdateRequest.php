<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:100', 'regex:/^[\sa-zA-Z-]*$/'],
            'password' => ['exclude_if:password,null', 'string', 'max:255', 'regex:/^[\w!@#$%^&*()-+]*$/'],
        ];
    }
}
