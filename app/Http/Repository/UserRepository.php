<?php


namespace App\Http\Repository;

use Illuminate\Support\Facades\DB;

class UserRepository
{

    public function datatable(array $request)
    {
        $users = DB::table('users')
            ->select('users.id', 'users.username', 'users.name', DB::raw("GROUP_CONCAT(`groups`.`name` SEPARATOR ', ') as `groups`"), 'users.created_at', 'users.updated_at')
            ->leftJoin('roles', 'users.id', '=', 'roles' . '.user_id')
            ->leftJoin('groups', 'roles.group_id', '=', 'groups.id')
            ->groupBy('users.id');
        $countBefore = $users->count();

        $columns = $request['columns'];
        $search = $request['search']['value'];
        if ($search) {
            $users->where(function ($q) use ($columns, $search) {
                $first = true;
                foreach ($columns as $value) {
                    if ($value['searchable'] == 'true' && $value['name']) {
                        if ($first) {
                            $q->where($value['name'], 'like', '%' . $search . '%');
                        } else {
                            $q->orWhere($value['name'], 'like', '%' . $search . '%');
                        }
                        $first = false;
                    }
                }
            });
        }

        $orderDir = $request['order'][0]['dir'];
        $orderColumn = $request['order'][0]['column'];
        if ($columns[$orderColumn]['orderable'] == 'true') {
            $users = $users->orderBy($columns[$orderColumn]['name'], $orderDir);
        } else {
            $users = $users->oldest();
        }

        $countAfter = $users->count();
        $users = $users->skip($request['start'])->take($request['length'])->get();

        return [
            'draw' => $request['draw'],
            'recordsTotal' => $countBefore,
            'recordsFiltered' => $countAfter,
            'data' => $users
        ];
    }
}
