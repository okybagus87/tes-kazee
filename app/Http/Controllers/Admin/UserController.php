<?php

namespace App\Http\Controllers\Admin;

use App\Http\Repository\UserRepository;
use App\Http\Requests\Admin\User\UserCreateRequest;
use App\Http\Requests\Admin\User\UserUpdateGroupRequest;
use App\Http\Requests\Admin\User\UserUpdateRequest;
use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController
{
    /**
     * @var UserRepository
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $groups = Group::all();
        return view('admin.user.index', ['groups' => $groups]);
    }

    public function datatable(Request $request)
    {
        return response($this->repository->datatable($request->all()));
    }

    public function delete(User $user_id)
    {
        $delete = $user_id->delete();
        if ($delete) {
            return response(['status' => true, 'message' => 'User deleted']);
        } else {
            return response(['message' => 'Failed to delete user'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    public function create(UserCreateRequest $request)
    {
        $request = $request->validated();

        $request['password'] = bcrypt($request['password']);

        $user = User::create($request);

        return response(['status' => true, 'data' => $user]);
    }

    public function get(User $user_id)
    {
        return response(['status' => true, 'data' => $user_id]);
    }

    public function getUserGroup(User $user_id)
    {
        $user = $user_id->append('group_ids');
        return response(['status' => true, 'data' => $user]);
    }

    public function updateUserGroup(User $user_id, UserUpdateGroupRequest $request)
    {
        $user = $user_id->groups()->sync($request->group_id);
        return response(['status' => true, 'data' => $user]);
    }

    public function update(User $user_id, UserUpdateRequest $request)
    {
        $request = $request->validated();

        if (isset($request['password'])) {
            $request['password'] = bcrypt($request['password']);
        }

        $user = $user_id->update($request);
        return response(['status' => true, 'data' => $user]);
    }
}
