<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\LoginPostRequest;
use App\Models\User;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    public function index()
    {
        if (auth()->check()) {
            return redirect()->route('admin.dashboard');
        }

        return view('login');
    }

    public function login(LoginPostRequest $request)
    {
        $user = User::where('username', $request->username)->first();

        if ($user) {
            if (auth()->attempt($request->only(['username', 'password']), $request->remember_me)) {
                $request->session()->regenerate();

                return response(
                    ['status' => true, 'redirect' => redirect()->intended(route('admin.dashboard'))->getTargetUrl()]
                );
            }

            return response(
                ['message' => 'Invalid username or password', 'redirect' => ''],
                Response::HTTP_BAD_REQUEST
            );
        }

        return response(['message' => 'Unauthorized user', 'redirect' => ''], Response::HTTP_BAD_REQUEST);
    }
}
