<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    use HasFactory;

    protected $table = 'roles';

    /**
     * The attributes thata are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id',
        'user_id',
    ];

    /**
     * Connection GroupUser Model with User Model
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Connection GroupUser Model wuth Group Model
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
