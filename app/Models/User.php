<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static function booted()
    {
        static::deleting(function (User $user) {
            if ($user->id == 1) return false;
        });
    }

    public function getGroupIdsAttribute()
    {
        return $this->groups->pluck('id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class, 'roles')->withTimestamps();
    }

    public function group_user()
    {
        return $this->hasOne(GroupUser::class);
    }
}
